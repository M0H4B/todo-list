﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TODO
{
    public partial class panelControl : UserControl
    {


        detailpanelControl Detail_control;
        public panelControl(APPOINTMENT ap)
        {
            InitializeComponent();
            Detail_control = new detailpanelControl(ap);

            this.checkBox1.Checked = ap.NOTIFY == null ? false : (bool)ap.NOTIFY;
            this.txttitle.Text = ap.APPOINTMENT_NAME;
        }
        bool is_visable = false;
        private void button2_Click(object sender, EventArgs e)
        {
            if (!is_visable)
            {
                flowLayoutPanel1.Controls.Add(Detail_control);
                this.Size = new Size(this.Size.Width, this.Size.Height + 200);

            }
            else {
                flowLayoutPanel1.Controls.Add(Detail_control);
                this.Size = new Size(this.Size.Width, this.Size.Height - 200);
            
            }
            is_visable = !is_visable;
        }
    }
}
