﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TODO
{
    public partial class detailpanelControl : UserControl
    {
        public detailpanelControl(APPOINTMENT ap)
        {
            InitializeComponent();

            this.txtNotes.Text = ap.APPOINTMENT_NOTE;
            dateTimePicker1.Value = (DateTime)ap.DUE_DATE;
        }
    }
}
