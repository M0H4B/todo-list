﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TODO
{
    public partial class listControl : UserControl
    {

        DataClasses1DataContext db;

        public listControl()
        {
            InitializeComponent();


            // load data

                         db = new DataClasses1DataContext("Data Source=46.32.114.205;Initial Catalog=Mohab-sh;User ID=mohab;Password=am8f8p5f");
             var tasks = db.TASKs.AsQueryable();
             if (tasks.Count() != 0)
             {
                 foreach (var t in tasks)
                 {
                     flowPanel.Controls.Add(new singleitemControl(t));

                 }
             }
             else {
                 panelnotask.BringToFront();
                 panelnotask.Visible = true;
             }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox2.Text.Trim() == "" || textBox2.Text == "List Name")
            {
                textBox2.Text = "";
                textBox2.Focus();
            }
            else
            {
                var task = new TASK() { ID = Guid.NewGuid(), TASK_NAME = textBox2.Text, INSERT_DATE = DateTime.Now, IS_DELETED = false };
                singleitemControl ctrl = new singleitemControl(task);
                flowPanel.Controls.Add(ctrl);

                db.TASKs.InsertOnSubmit(task);
                db.SubmitChanges();
                textBox2.Text = "List Name";
                flowPanel.Focus();
                panelnotask.SendToBack();
                panelnotask.Visible = false;
            }
        }
    }
}
