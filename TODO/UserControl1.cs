﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevComponents.Schedule.Model;
using DevComponents.DotNetBar.Schedule;

namespace TODO
{
    public partial class UserControl1 : UserControl
    {
        DataClasses1DataContext db;
        public UserControl1()
        {
            InitializeComponent();
            CalendarModel _Model = new CalendarModel();
            calendarView1.CalendarModel = _Model;

            db = new DataClasses1DataContext();
            var appointments = db.APPOINTMENTs.AsQueryable();

            foreach (var item in appointments)
            {
                _Model.Appointments.Add(new Appointment() {
                    Subject = item.APPOINTMENT_NAME,
            StartTime = item.DUE_DATE,
                    EndTime = item.DUE_DATE.AddMinutes(3),
            CategoryColor = Appointment.CategoryPurple,
            TimeMarkedAs = Appointment.TimerMarkerBusy,Tag=item
                
                });
                
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            calendarView1.SelectedView = DevComponents.DotNetBar.Schedule.eCalendarView.Day;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            calendarView1.SelectedView = DevComponents.DotNetBar.Schedule.eCalendarView.Week;

        }

        private void button3_Click(object sender, EventArgs e)
        {
            calendarView1.SelectedView = DevComponents.DotNetBar.Schedule.eCalendarView.Month;

        }

        private void calendarView1_AppointmentViewChanged(object sender, AppointmentViewChangedEventArgs e)
        {
        }
    }
}
